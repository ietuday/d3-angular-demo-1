# D3AngularDemo1

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.1.5.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

# D3.js with Angular Examples

This project shows how to integrate D3.js with Angular.

We'll implement a few D3.js examples described in [bl.ocks.org](http://bl.ocks.org/) in Angular

![Example1](./resources/Screenshot1.png "Example1")
![Example2](./resources/Screenshot2.png "Example2")
![Example3](./resources/Screenshot3.png "Example3")
![Example4](./resources/Screenshot4.png "Example4")
![Example5](./resources/Screenshot5.png "Example5")
![Example6](./resources/Screenshot6.png "Example6")
![Example7](./resources/Screenshot7.png "Example7")

## Examples Summary

- [Line Chart 4.0](http://bl.ocks.org/mbostock/02d893e3486c70c4475f)
- [Multi-Series Line Chart](http://bl.ocks.org/mbostock/3884955)
- [Bar Chart](http://bl.ocks.org/mbostock/3885304)
- [Stacked Bar Chart](https://bl.ocks.org/mbostock/3886208)
- [Brush & Zoom](https://bl.ocks.org/mbostock/34f08d5e11952a80609169b7917d4172)
- [Pie Chart](http://bl.ocks.org/mbostock/3887235)
- [Donut Chart](https://bl.ocks.org/mbostock/3887193)


